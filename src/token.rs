
use std::collections::HashMap;

extern crate regex;

pub enum OperatorType {
    Plus,
    Subtract,
    Multiply,
    Divide,
    Assignment,
    Access,
}

pub enum Token {
    Integer(i32),
    Operator(OperatorType),
    Str(String),
}

impl Token {
    pub fn tokenize (expression: &str) -> Vec<Token> {

        let split_by_whitespace = regex::Regex::new(r"\s+").unwrap();
        let mut tokens: Vec<Token> = Vec::new();

        for word in split_by_whitespace.split(expression) {
            match word {
                "+" => {
                    tokens.push(Token::Operator(OperatorType::Plus));
                },

                "-" => {
                    tokens.push(Token::Operator(OperatorType::Subtract));
                },

                "*" => {
                    tokens.push(Token::Operator(OperatorType::Multiply));
                },

                "/" => {
                    tokens.push(Token::Operator(OperatorType::Divide));
                },

                "=" => {
                    tokens.push(Token::Operator(OperatorType::Assignment));
                },

                "@" => {
                    tokens.push(Token::Operator(OperatorType::Access));
                },

                _ => {
                    if word.is_empty() {
                        continue;  
                    }

                    match word.trim().parse::<i32>() {
                        Ok(num) => {
                            tokens.push(Token::Integer(num));
                        },

                        Err(_) => {
                            tokens.push(Token::Str(word.to_string()));
                        },
                    };
                }
            }
        }

        tokens
    }

    pub fn parse_tokens (tokens: &Vec<Token>) -> Option<i32> {
        let mut stack: Vec<i32> = Vec::new();
        let mut string_stack: Vec<String> = Vec::new();

        let mut table = HashMap::new();

        for token in tokens {
            match token {
                Token::Integer(number) => {
                    stack.push(*number); 
                },

                Token::Str(string) => {
                    string_stack.push(string.clone());    
                },

                Token::Operator(ref op) => {
                    match op {
                        OperatorType::Plus => {
                            let operand2 = stack.pop()
                                .expect("Not enough operands for addition!");
                            let operand1 = stack.pop()
                                .expect("Not enough operands for addition!");
    
                            stack.push(operand1 + operand2);
                        },
    
                        OperatorType::Subtract => {
                            let operand2 = stack.pop()
                                .expect("Not enough operands for subtraction!");
                            let operand1 = stack.pop()
                                .expect("Not enough operands for subtraction!");
    
                            stack.push(operand1 - operand2);
                        },
    
                        OperatorType::Multiply => {
                            let operand2 = stack.pop()
                                .expect("Not enough operands for multiplication!");
                            let operand1 = stack.pop()
                                .expect("Not enough operands for multiplication!");
    

                            stack.push(operand1 * operand2);
                        }, 
                     
                        OperatorType::Divide => {
                            let operand2 = stack.pop()
                                .expect("Not enough operands for division!");
                            let operand1 = stack.pop()
                                .expect("Not enough operands for division!");
    
                            if operand2 == 0 {
                                panic!("Attempted division by 0");
                            }

                            stack.push(operand1 / operand2);
                        },

                        OperatorType::Assignment => {
                            let name = string_stack.pop()
                                .expect("Not enough string operands for assignment");
                            let value = stack.pop()
                                .expect("Not enough operands for assignemtn");

                            // if already exists, overrides
                            table.insert(name, value);
                        },

                        OperatorType::Access => {
                            let name = string_stack.pop()
                                .expect("Not enough string operands for access!");

                            if table.contains_key(&name) {
                                stack.push(*table.get(&name).unwrap());
                            } else {
                                panic!("The Symbol {} does not exist!", name);
                            }
                        },
                    }
                },
            }
        }

        stack.pop()
    }
}
