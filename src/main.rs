
mod token;

use std::io::{self, Write};
use token::*;

fn main () {
    let mut user_input = String::new();

    print!("> ");
    io::stdout().flush().unwrap();
    io::stdin().read_line(&mut user_input)
        .expect("Failed to read user input!");

    let expr = user_input.trim().to_string();

    let tokens = Token::tokenize(&expr);
    let result = Token::parse_tokens(&tokens);

    match result {
        Some(number) => {
            println!("{}", number);
        },
        None => {
            println!("No result.");
        }
    }
}

